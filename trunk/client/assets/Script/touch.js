// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        cameraNode: {
            displayName: '地图摄像机',
            type: cc.Camera,
            default: null
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.node.on(cc.Node.EventType.MOUSE_WHEEL, this.onMouseWheel, this);
        //点击事件
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        //点击事件
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchBegan, this);
    },

    start() {

    },

    onMouseWheel(event) {
        let scrollY = event.getScrollY();
        this.cameraNode.node.z += scrollY;
    },

    onTouchMove(event) {
        //this.cameraNode.getComponent('MyCamera').MyScreenToWorld(endP);
        let delta_pos = event.getDelta().mul(-1);
        let cameraNode = this.cameraNode.node;
        cameraNode.x = cameraNode.x - delta_pos.x;
        cameraNode.y = cameraNode.y - delta_pos.y;
        //console.log('位置...', cameraNode.x, cameraNode.y);
        cc.game.emit('CHECK_LEAGUE_ATTACK_IS_OK', delta_pos);
    },

    onTouchBegan(event) {

    },
    // update (dt) {},
});
