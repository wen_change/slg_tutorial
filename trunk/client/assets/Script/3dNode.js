// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        sprite: cc.Sprite,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {
        //this.update3DAll(this.node);
        cc.loader.loadRes('world/world_map_resources', cc.SpriteAtlas, (error, res) => {
            this.sprite.spriteFrame = res.getSpriteFrame('main_city1');
            this.update3DAll(this.node);
        });
    },

    update3DAll(node) {
        node.is3DNode = true;
        node.children.forEach((child) => {
            this.update3DAll(child);
        });
    },

    update(dt) {
        this.checkFace();
        this.node.zIndex = Math.floor((97408 - this.node.y) / 10);
    },

    checkFace() {
        let camera = cc.Camera.findCamera(this.node);
        if (!camera) return;
        let p = camera.node.convertToWorldSpaceAR(cc.v3(0, 0, 0));
        let node = this.node;
        node.lookAt(p, cc.v3(0, 0, 1));
        let lp3 = node.eulerAngles;
        lp3.z = 0;
        node.eulerAngles = lp3;
        node.scaleX = -1;
    },
});
