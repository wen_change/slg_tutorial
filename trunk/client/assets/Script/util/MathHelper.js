
let MathHelper = {
    /**
     * 获取起点到终点的弧度
     * @param {cc.Vec2} start 起点坐标
     * @param {cc.Vec2} end 终点坐标
     * @returns {number} 弧度
     */
    getRad(start, end) {
        let vector = cc.v2(1, 0);
        let offset_pos = cc.v2(end.x, end.y).sub(cc.v2(start.x, start.y));
        let magSqr1 = offset_pos.magSqr();
        let magSqr2 = vector.magSqr();
        if (magSqr1 === 0 || magSqr2 === 0) {
            return 0;
        }
        let angle = offset_pos.signAngle(vector);
        //cc.log('pos:', offset_pos,'rad', angle,'angle:', angle * 180 / Math.PI );
        return angle;
    },

    /**
     * 获取起点到终点的角度
     * @param {cc.Vec2} start 起点坐标
     * @param {cc.Vec2} end 终点坐标
     * @returns {number} 角度
     */
    getAngle(start, end) {
        let rad = this.getRad(start, end);
        return rad * 180 / Math.PI;
    },

};

module.exports = MathHelper;

