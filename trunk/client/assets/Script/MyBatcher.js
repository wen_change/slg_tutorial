// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
let BatcherData = cc.Class({
    name: 'MyBatcherData',
    properties: {
        name: {
            displayName: '名字',
            default: ''
        },
        index: {
            displayName: '序号',
            default: 0
        },
    },
});
cc.Class({
    extends: cc.Component,
    properties: {
        m_monitorContent: {
            displayName: '监听容器',
            default: null,
            type: cc.Node,
        },
        m_isAutoBatchRich: {
            displayName: '富文本批处理',
            default: true
        },
        m_isAutoBatch: {
            displayName: '是否自动批处理',
            default: true
        },
        m_customBatchList: {
            default: [],
            type: cc.String
        },
        m_customBatchList2: {
            displayName: 'map列表优先级高',
            default: [],
            type: BatcherData
        },
        m_isEnable: {
            displayName: '是否启用',
            default: false
        }
    },
    // LIFE-CYCLE CALLBACKS:
    onLoad() {
        if (!this.m_isEnable) return;
        this.initConfig();
        this.checkPlatform();
        cc.game.on('INIT_AUTO_BATCH_LEVEL', this.checkPlatform, this);

        // 监听容器子节点新增事件
        if (!this.m_monitorContent) {
            let scroll = this.getComponent(cc.ScrollView);
            if (scroll && cc.isValid(scroll.content)) {
                this.m_monitorContent = scroll.content;
            }
        }
        if (this.m_monitorContent) {
            this.m_monitorContent.on(cc.Node.EventType.CHILD_ADDED, this.checkPlatform, this);
        }
    },
    initConfig() {
        if (!this.m_isEnable) return;
        this._indexMax = 0;
        this.m_customBatchMap = {};
        let list = [];
        if (this.m_customBatchList2.length > 0) {
            this.m_customBatchList = [];
            for (let i = 0; i < this.m_customBatchList2.length; i++) {
                let data = this.m_customBatchList2[i];
                if (this.m_customBatchMap[data.name]) {
                    console.log('这个名字重名:', data.name);
                }
                if (list[data.index]) {
                    console.log('这个名字序号重复:', data.index);
                }
                this.m_customBatchMap[data.name] = data.index;
                list[data.index] = true;
                this.m_customBatchList[data.index] = data.name;
            }
        } else {
            for (let i = 0; i < this.m_customBatchList.length; i++) {
                let key = this.m_customBatchList[i];
                this.m_customBatchMap[key] = i;
                if (!this.checkIsNameInChild(key, this.node)) {
                    //console.log('没有这个名字的子类:', key);
                }
            }
        }
        Object.keys(this.m_customBatchMap).forEach((key) => {
            let index = this.m_customBatchMap[key];
            if (index > this._indexMax) {
                this._indexMax = index;
            }
        });
    },
    updateRenderLevel(node) {
        if (!this.m_isEnable) return;
        let level = this.m_customBatchMap[node.name];
        if (level) {
            node._proxy.updateRenderLevel(level);
        } else {
            this._curAddIndex++;
            node._proxy.updateRenderLevel(this._indexMax + this._curAddIndex);
        }
        node.children.forEach((child) => {
            this.updateRenderLevel(child);
        });
    },

    checkPlatform() {
        if (!this.m_isEnable) return;
        if (cc.sys.isNative && cc.isValid(this)) {
            this._curAddIndex = 0;
            this.node._proxy.autoBatch();
            this.updateRenderLevel(this.node);
        }
    },

    checkIsNameInChild(name, c) {
        if (!this.m_isEnable) return false;
        let _children = c._children;
        let count = _children.length;
        for (let i = 0; i < count; i++) {
            let cc = _children[i];
            if (cc.name === name) {
                return true;
            } else {
                return this.checkIsNameInChild(name, cc);
            }
        }
        return false;
    },

    start() {

    },

    onDestroy() {
        if (!this.m_isEnable) return;
        cc.game.off('INIT_AUTO_BATCH_LEVEL', this.checkPlatform, this);
    }
    // update (dt) {},
});
